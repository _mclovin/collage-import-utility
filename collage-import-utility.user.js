// ==UserScript==
// @name           Gazelle: Collage Import Utility
// @description    Find groups based on output of the Collage Export Utility (https://greasyfork.org/en/scripts/39291-redacted-collage-export-utility/code)
// @author         _
// @version        0.8
// @grant          GM.xmlHttpRequest
// @connect        redacted.sh
// @connect        orpheus.network
// @connect        notwhat.cd
// @match          https://redacted.sh/collages.php?id=*
// @match          https://orpheus.network/collages.php?id=*
// @run-at         document-end
// ==/UserScript==

(() => {
  "use strict";

  // EDIT BELOW THIS LINE
  const EXCLUDES = [":", "Vol\\."]; // If including special (regex) chars in this list, e.g. [, |, \, ^, they must be double-escaped.
  // EDIT ABOVE THIS LINE

  const add_button = [...document.querySelectorAll(".submit_div")][1];
  if (add_button == undefined) return; // not permitted to manage this collage; bail.
  const searchButton = document.createElement("input");
  searchButton.type = "submit";
  searchButton.value = "Search";
  const checkbox = document.createElement("input");
  checkbox.type = "checkbox";
  checkbox.id = "checked_classical";
  checkbox.name = "checked_classical";

  document.querySelector("span.float_right > a.brackets").text = "Individual add";
  document.querySelectorAll(".add_torrent_container").forEach(elem => elem.classList.toggle("hidden"));
  add_button.insertAdjacentElement("beforeend", searchButton);
  add_button.insertAdjacentElement("beforeend", checkbox);

  document.querySelectorAll("form.add_form > span")[1].innerText =
    "Check the box if importing classical.\n\nEnter the URLs of torrent groups on the site, one per line.";

  const divURLImportBox = `
    <div class="box box_addtorrent">
      <div class="head"><strong>Import Gazelle Collage</strong></div>
      <div class="pad add_torrent_container">
          <form id="import-collage" class="add_form">
              <div class="field_div">
                  <input id='import-url' type="text" size="20" name="url" />
              </div>
              <div class="submit_div">
                  <input id="import-button" type="submit" value="Import" />
                  <input id="clear-api" type="button" value="Clear API Keys" />
              </div>
              <span style="font-style: italic;">Enter the URL of collage to import.</span>
          </form>
      </div>
    </div>
  `;

  document.querySelector('.box_addtorrent').insertAdjacentHTML('afterend', divURLImportBox);

  const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

  const clearAPIKey = () => {
    for (let key in localStorage) {
      if (key.endsWith("-apikey")) {
        localStorage.removeItem(key);
      }
    }
    alert("API Keys cleared.")
  };

  const corsFetchJSON = (url, apiKey) => {
    return new Promise((resolve, reject) => {
      GM.xmlHttpRequest({
        method: "GET",
        url: encodeURI(url),
        headers: {
          Authorization: apiKey,
        },
        onload: res => resolve(JSON.parse(res.responseText)),
        onerror: res => reject(res)
      });
    });
  };

  const htmlDecode = (text) => {
    const doc = new DOMParser().parseFromString(text, "text/html");
    return doc.documentElement.textContent;
  };

  const normalize = (str) => {
    return str.replace(/[.,\/#–!$%\^&\*;:{}=\-_—`~()]/g,"").replace(/\s{2,}/g," ");
  }

  const gazelleAPI = async (artist, album) => {
    try {
      if (artist.includes("Various Artists") || checkbox.checked) {
        artist = "";
      }
      const res = await fetch(`/ajax.php?action=browse&artistname=${encodeURIComponent(artist)}&groupname=${encodeURIComponent(album)}`);
      return res.json();
    } catch (error) {
      console.log(error);
    }
  };

  const importCollage = async event => {
    event.preventDefault();
    const importButton = document.getElementById('import-button')
    importButton.disabled = true
    importButton.value = "Processing ...";
    const textBox = document.querySelector(".field_div > textarea");
    const regex = /https:\/\/(.+)\/collages?\.php\?id=(\d+)/;
    const collageURL = document.getElementById('import-url').value;
    if (!regex.test(collageURL)) {
      alert("Invalid collage URL.")
      importButton.disabled = false
      importButton.value = "Import";
      return;
    }
    const [, hostname, collageID] = collageURL.match(regex);
    let apiKey = localStorage.getItem(`${hostname}-apikey`);
    if(!apiKey){
        apiKey = prompt(`Please enter API key for ${hostname}.`);
        if (apiKey) {
          localStorage.setItem(`${hostname}-apikey`, apiKey);
        } else {
          importButton.disabled = false
          importButton.value = "Import";
          return;
        }
    }
    const collageJSON = await corsFetchJSON(`https://${hostname}/ajax.php?action=collage&id=${collageID}`, apiKey);
    let releases;
    try {
      if (collageJSON.error) {
        console.error(collageJSON.error);
        alert(`API Error (${hostname}): ${collageJSON.error}`);
        importButton.disabled = false;
        importButton.value = "Import";
        return;
      }
      releases = collageJSON.response.torrentgroups;
    } catch (e) {
      console.log(e);
      alert("Failed. Check connection or API key.")
      importButton.disabled = false;
      importButton.value = "Import";
      return;
    }
    let iterations = releases.length;
    let count = 0;
    let numFound = 0;
    for (let release of releases) {
      let found = false;
      let artist, album, year;
      try {
        artist = htmlDecode(release.musicInfo.artists[0].name);
        album = htmlDecode(release.name);
        year = release.year;
        EXCLUDES.forEach(exclusion => {
          let re = new RegExp(exclusion, "g");
          artist = artist.replace(re, "");
          album = album.replace(re, "");
        });
      } catch (e) {
        console.log(e);
        continue;
      }
      await wait(2000);
      const { response, status } = await gazelleAPI(artist, album);
      count += 1;
      importButton.value = `Processing ... ${count}/${iterations}`;
      if (status !== "success") {
        console.log(`NOT FOUND: ${artist} - ${album} (${year}) (API request failed)`);
        return;
      }
      for (let group of response.results) {
        EXCLUDES.forEach(exclusion => {
          let re = new RegExp(exclusion, "g");
          group.groupName = group.groupName.replace(re, "");
        });
        if (group.groupYear == year && normalize(htmlDecode(group.groupName)).toLowerCase() == normalize(album).toLowerCase()) {
          textBox.value += `${location.origin}/torrents.php?id=${group.groupId}\r\n`;
          textBox.scrollTop = textBox.scrollHeight
          found = true;
          numFound += 1;
          break;
        }
      }
      if (!found) {
        console.log(`NOT FOUND: ${artist} - ${album} (${year})`);
      }
    }
    importButton.value = `Done | ${numFound}/${iterations} found.`;
  };

  const searchForGroups = async event => {
    event.preventDefault();
    const textBox = document.querySelector(".field_div > textarea");
    const releases = textBox.value.split("\n");
    if (releases[0].startsWith("https")) return;
    event.target.disabled = true;
    event.target.value = "Processing ...";
    textBox.value = "";
    let iterations = releases.length;
    let count = 0;
    let numFound = 0;
    for (let release of releases) {
      let found = false;
      let tokens, artist, album, year;
      try {
        tokens = release
          .replace(/ - /, "UNIQ_SEPARAT0R")
          .split("UNIQ_SEPARAT0R");
        artist = tokens[0].replace(/\[?\(?\?+\)?\]?/g, "");
        album = tokens[1]
          .replace(/ \[\d{4}\]/, "")
          .replace(/\[?\(?\?+\)?\]?/g, "");
        year = tokens[1].match(/\[(\d+)*\]/)[1];
        EXCLUDES.forEach(exclusion => {
          let re = new RegExp(exclusion, "g");
          artist = artist.replace(re, "");
          album = album.replace(re, "");
        });
      } catch (e) {
        console.log(e);
        console.log(`NOT FOUND: ${artist} - ${album} (${year}) (Unexpected format)`);
        continue;
      }
      await wait(2000);
      const { response, status } = await gazelleAPI(artist, album);
      count += 1;
      event.target.value = `Processing ... ${count}/${releases.length}`;
      if (status !== "success") {
        console.log(`NOT FOUND: ${artist} - ${album} (${year}) (API request failed)`);
        return;
      }
      for (let group of response.results) {
        EXCLUDES.forEach(exclusion => {
          let re = new RegExp(exclusion, "g");
          group.groupName = group.groupName.replace(re, "");
        });
        if (group.groupYear == year && normalize(htmlDecode(group.groupName)).toLowerCase() == normalize(album).toLowerCase()) {
          textBox.value += `${location.origin}/torrents.php?id=${group.groupId}\r\n`;
          textBox.scrollTop = textBox.scrollHeight
          found = true;
          numFound += 1;
          break;
        }
      }
      if (!found) {
        console.log(`NOT FOUND: ${artist} - ${album} (${year})`);
      }
    }
    event.target.value = `Done | ${numFound}/${iterations} found.`;
  };

  searchButton.addEventListener("click", searchForGroups);
  document.getElementById("import-collage").addEventListener("submit", importCollage)
  document.getElementById("clear-api").addEventListener("click", clearAPIKey);
})();
